#!/bin/bash
cd cldemo2/simulation

vagrant up oob-mgmt-server oob-mgmt-switch

echo "Copy Topology Automation to oob-mgmt-server"
vagrant scp ../../automation oob-mgmt-server:/home/vagrant

if [ "$1" == "netq" ]; then
  vagrant up netq-ts
fi

vagrant up leaf01 leaf02 leaf03 leaf04 spine01 spine02 spine03 spine04 
vagrant up server01 server02 server03 server04 
vagrant up server05 server06 server07 server08 
vagrant up border01 border02 fw1 fw2


